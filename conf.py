#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html


# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('.'))
sys.path.insert(0, os.path.abspath('ext'))


# -- Project information -----------------------------------------------------

project = 'Math and Science Working Groups'
copyright = '2020-2024, Mark Galassi'
author = 'Mark Galassi'

# The short X.Y version
version = '2024.01.10'

# The full version, including alpha/beta/rc tags
release = '2024.01.10'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
## note that I have a special trick to use mathjax for html and
## latexpdf math, while using pngmath for epub output
math_extension = 'sphinx.ext.mathjax'
import sys
# if 'epub' in sys.argv:
#     math_extension = 'sphinx.ext.pngmath'
print(sys.argv)
print('USING math extension:', math_extension)
extensions = [
    # 'sphinx_lesson.directives',
    # 'sphinx.ext.autosectionlabel',
    'numbered_block',
    'sphinxcontrib.bibtex',
    'sphinxcontrib.programoutput',
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.intersphinx',
    math_extension,
    'sphinx.ext.todo',
    'sphinx.ext.ifconfig',
    'sphinx.ext.graphviz',
    'sphinx.ext.viewcode',
    # 'sphinx_multitoc_numbering',
]
bibtex_bibfiles = ['working_groups.bib']
bibtex_reference_style = 'author_year'

## numbered_blocks options:
numbered_blocks = \
    [
        {'name': 'example'},
        {'name': 'solution', 'numbered': False,
         'wrap-content':
         '<div class="contents">%s</div>\n<button onclick="showhide(this)">Solution</button>\n'},
        {'name': 'exerciseL'},
        {'name': 'exercise'},
        {'name': 'definition', 'reference-name': 'defref',
         'title-format': '(%s)', 'title-separator': ' '},
        {'name': 'fake-figure', 'counter': 'figure',
         'title-position': 'bottom', 'label-format': 'Figure %s',
         'numbering-style': 'Alpha', 'numbering-level': 2},
    ]

## numfig options:
numfig = True
numfig_secnum_depth = 2
numfig_format = {'figure': 'Figure %s',
                 'table': 'Table %s',
                 'code-block': 'Listing %s',
                 'section': 'Section %s'}

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store',
                    #'substitutions.rst'
                    ]



# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# html_theme = 'alabaster'
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# rst_epilog = """
# .. include:: substitutions.rst
# """

# -- Options for pdflatex output --------------------------
my_latex_authors = 'Mark Galassi'
my_latex_preamble = """\\usepackage{cancel}
\\usepackage{underscore}
"""
latex_elements = {
    # The paper size ('letterpaper' or 'a4paper').
    #
    # 'papersize': 'letterpaper',

    # The font size ('10pt', '11pt' or '12pt').
    #
    # 'pointsize': '10pt',

    # Additional stuff for the LaTeX preamble.
    #
    'preamble': my_latex_preamble,

    # Latex figure (float) alignment
    #
    # 'figure_align': 'htbp',
}
latex_documents = [
    ('index', 'MathandScienceWorkingGroups.tex',
     'Math and science working groups',
     my_latex_authors, 'manual')
]


# -- Extension configuration -------------------------------------------------
