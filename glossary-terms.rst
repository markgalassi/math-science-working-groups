****************
 Glossary Terms
****************

.. |RTD| replace:: Read the Docs

.. |RST| replace:: reStructured Text

.. glossary::

  Monomial
  Monomials
    An expression that multplies and divides numbers and letters with
    no + or - signs.  Examples: :math:`x^2`, :math:`a b`, :math:`3 a
    y`, :math:`2.2 x / (a b y)`.

  Binomial
  Binomials
    A sum or subtraction of two :term:`monomials`.  Examples:
    :math:`x^2 - x`, :math:`x^2 - x`, :math:`x^2 - 4`, :math:`1 + a`,
    :math:`a + b`, :math:`a^2 - b^2`, :math:`b + x^7`,
    :math:`c + 1`, :math:`a x^m + b x^n`, :math:`0.9x^{3}+\pi y^{2}`.

  Factoring
    Factoring of an integer is to write it as a product of smaller
    numbers.  Factoring of a polynomial is to write it as a product of
    smaller polynomials.

  RST
    |RST| is an easy-to-read, what-you-see-is-what-you-get plaintext
    markup syntax and parser system. It is useful for in-line program
    documentation (such as Python docstrings), for quickly creating
    simple web pages, and for standalone documents. |RST| is designed
    for extensibility for specific application domains. The |RST|
    parser is a component of Docutils.

  Sphinx
    Sphinx is a tool that makes it easy to create intelligent and
    beautiful documentation. It was originally created for the Python
    documentation, and it has excellent facilities for the
    documentation of software projects in a range of languages.
