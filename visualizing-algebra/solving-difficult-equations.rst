.. _chap-solving-difficult-equations:

*****************************
 Solving difficult equations
*****************************

The Youtube channel SyberMath has several interesting equations,
including rational, irrational, and transcendental.  Here we will show
some of them so that we can solve them together.

Repeated viewing of technique used should get us to the point where we
know how to tackle them.

The channel is at https://www.youtube.com/@SyberMath

Polynomial equations
====================

.. math::

   x^6 = (x-1)^6
   \\
   (a+b)^5 = a^5 + b^5


Rational equations
====================

Simplify:

.. math::

   \frac{2\sqrt{6}}{\sqrt{2}+\sqrt{3}+\sqrt{5}} + \sqrt{5}


Irrational equations
====================

.. math::

   \sqrt{x+8} - \sqrt{x} = 2

Transcendental equations
========================

.. math::

   (\ln x)^5 = \ln x^5
   \\
   2^{x^{2}} = 3^x
   \\
   (x^2 - x - 1)^{x+2} = 1



Functional equations
====================

.. math::

   f\left(\frac{2x-1}{x-3}\right) = x^2
   \\
   x = \frac{3y-1}{y-2}

