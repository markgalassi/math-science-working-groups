.. _app-visualizing-algebra-sample-lesson-plans:

*****************************************************
 Appendix: visualizing algebra - sample lesson plans
*****************************************************

Teaching the "visualizing algebra" material can clearly be sliced up
in many different ways.  I have taught it several times, and I always
followed a similar logistical format: class length of 1 hour and 15
minutes, and new material once/week, with catch-up sessions
twice/week.

But the order in which I teach the various pieces, and the choice of
which pieces to teach, depends largely on the makeup of the group of
students I work with.

Similarly, the amount of extra background material and the choice of
"visions into advanced material" anecdotes depend on the group of
students I have.

In this appendix I will start collecting some of the specific lesson
plan sequences I have chosen.

.. _sec-visualizing-algebra-plan-fall-2024:

Fall 2024
=========

We had a total of 10 lessons, from 2024-10-07 to 2024-12-16.

.. rubric:: Opening lesson 1:

* Introduce myself, discuss how researchers see mathematics
* Discussion of logistics and what I expect of students
* Section "The why of messy exponents"
* Section "An example to whet your appetite"

.. rubric:: Lesson 2:

* Review from OpenSTAX book: "A nostalgic romp through coordinates and
  plotting" - do the exercises suggested in the section.
* Review from OpenStax book: "What are functions?" - do the exercises
  suggested in the section.
* Section "Special powers of binomials"
* If we have time, start with sympy and introduce ``expand((a + b) ^
  7)`` and so forth.

.. rubric:: Lesson 3:

* Start with sympy, using the expressions in
  :numref:`chap-introducing-symbolic-algebra`

.. rubric:: Lesson 4:

Return to the "review of prerequisites" with the following sections:

* Reviewing fractions - what, seriously??
* Getting comfortable with visualizing functions
* Quadratic equations
* Some heavy emphasis on how functions are *constraints*

Remember to really lay it thick on how each equation reduces the
dimensionality of the space.

.. rubric:: Lesson 5:

We are now well beyond the review section, and we can spend a brief
amount of time on:

* Visualizing functions, using :numref:`chap-visualizing-functions` --
  the purpose here is to get used to 2D and 3D plots in Desmos or
  whatever other online graphing calculator we use.

Then we spend most of our time on:

* The pantheon of functions, using
  :numref:`chap-pantheon-of-functions`

.. rubric:: Lesson 6:

We continue with The Pantheon of Functions, moving from polynomials to
rational functions - using :numref:`chap-pantheon-of-functions`

.. rubric:: Lesson 9:

Fitting functions through points - :numref:`chap-fitting`

First show how fitting works, with constant reminders of how we
*impose* that a function with free parameters *must* pass through a
collection of points, and how this gives us a system of equations that
we can solve.

After working through this chapter we move to the book on programming
mini courses and look at how fitting works for a collection of data
points, and I discuss how overfitting can go wrong.

.. rubric:: Lesson 10:

We discuss, with a lot of examples, how dimensional reduction works -
:numref:`chap-visualizing-functions`
