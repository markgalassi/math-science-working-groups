(x^3)^2 - [(x-1)^3]^2 = 0
a^3 - b^3 = (a+b) (a-b)

a^3 + b^3 = (a+b) (a^2 - ab + b^2)
a^3 - b^3 = (a-b) (a^2 + ab + b^2)

====

2^x^2 = 3^x
x = 0
if x != 0:

Since 2^x^2 = (2^x)^x, we get:

(2^x)^x = 3^x

which implies:

2^x = 3
log 2^x = 3
x log 2 = log 3
x = log 3 / log 2

=-===

   \sqrt{x+8} - \sqrt{x} = 2

examine the conjugate and call it B:

(\sqrt{x+8} + \sqrt{x}) = B

(\sqrt{x+8} - \sqrt{x}) * (\sqrt{x+8} + \sqrt{x}) = 2 * B

which leads to B = 4

now subtract the two conjugates:

(\sqrt{x+8} - \sqrt{x}) - (\sqrt{x+8} + \sqrt{x}) = 2 - 4

2*\sqrt{x} = 2

x = 1
