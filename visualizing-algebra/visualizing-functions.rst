.. _chap-visualizing-functions:

***********************
 Visualizing functions
***********************

Here we will discuss visualizing functions, rather than data (which is
covered in :numref:`chap-data-visualization`).  We will discuss what
programs can do this, and then take a tour of some examples and what
insight we get from those examples.

We will have two goals, beyond getting comfortable with plotting
software: (a) learning how functions appear visually, and (b) looking
at how the intersection of equations gives a lower dimensional space,
and what that space looks like.

Command-line and web
====================

We can either plot on the command line, or use a web-based plotting
system.

On the command line I recommend gnuplot for "quickies": it allows
simple plotting of functions.  On Linux systems it is already there
for ready package installation; on other operating systems one will
have to download it, or use a third party packaging approach (such as
homebrew) to install it.

On the web there are two widely used system: GeoGebra and Desmos.
Both work well for simple line plots, but at this time (2022-12-12)
only GeoGebra offers surface plots.  GeoGebra is also based on
Free/Open-Source software, making it an excellent choice overall.

For the first few examples we will give both the instruction in
gnuplot and the GeoGebra instruction, and after a while we will switch
to simply showing the gnuplot instructions.

Overall I recommend getting used to command line programs rather than
web sites, since such approaches can be scripted and are reproducible.

Reduction in dimension: line plots
==================================

Remember that we are always looking at the reduction in dimension that
functions bring, so let us start by plotting some simple functions.

::

   $ gnuplot
   # then at the gnuplot prompt:
   plot x**2
   replot -2*x + 3

In GeoGebra at https://www.geogebra.org/calculator you can type in the
two equations:

::

   f(x) = x^2
   g(x) = -2 x + 5

after which you will have to zoom out a bit and shift the graph down
to find all the points at which the line and the parabola intersect.

**Exercise**

in both approaches, learn to zoom in and out.  Then, zooming in, find
the :math:`(x, y)` values of the two points at which the line and
parabola intersect.  Compare them to what you would get with the
quadratic formula.

Finally: notice how we start with the plane (all :math:`(x, y)`
points), which has dimension 2.  The first equation reduces the
:math:`(x, y)` pairs to just those on the parabola, which has
dimension 1. Finally the second equation brings it down to just two
points, each of which has dimension zero.


Reduction in dimension: surface plots
=====================================

::

   $ gnuplot
   # then at the gnuplot prompt:
   reset
   set zrange [-2:32]
   set pm3d
   set samples 150
   set isosamples 60
   set hidden3d
   splot 30*exp(-(x**2 + y**2) / 10)
   replot x + y
   replot 2


In GeoGebra at https://www.geogebra.org/calculator you can type in the
equations below:

::

   z = 30 exp(-(x^2+y^2)/10)

and zoom out until you can see all the interesting parts of this
curve.  You can also input these into the new Desmos 3D page at
https://www.desmos.com/3d

Notice how this has reduced the 3D Eclidean space down to a curved 2D
surface.

Now add a second equation which defines a different 2D surface (in this case a plane):

::

   z = x + y

Now rotate around so that you get a feeling for where the two surfaces
intersect.  You will see that it is a curved line.

Finally add a third equation:

::

   z = 10

You will see that this flat horizontal plane will intersect both of
the others - in intersecting the former it will define a circle, and
in the latter (since they are two flat planes) it will define a
straight line.

But the :math:`z = 10` surface is too high up to intersect *both* of
the other surfaces.  We can lower it by changing the 10 to a 3:

::

   z = 3

and now, if you rotate long enough you will find that there are two
*points* where all three surfaces intersect.

A simpler but less intriguing set of plots might come from:

::

   z = x + y
   z = x - y
   z = -x - y + 4

If you go and make it a bit harder, possibly with:

::

   z = 2x + 3 y
   z = x - 4 y
   z = -x - 3 y + 4

then you can compare the solution you get to how sympy might solve the
equations.
