*****************************
 Tour of polynomial geometry
*****************************

Here we will take a tour of various polynomials.  We will do so
interactively, using plotting programs (like gnuplot or geogebra), and
occasionally also look at their algebraic properties.

For each polynomial we study we will look at:

special points
   roots, intercept, vertices
factorization
   factor the polynomial, using both symbolic and numerical approaches


Second degree polynomials
=========================

.. math::

   f(x) = 0.1 x^2 - 1.5 x + 5

Plot it by inputing ``0.1*x**2 - 1.5*x + 5`` in geogebra.

Then look at making those numbers cleaner by myltiplying it by 10.

Now you can factor it by hand, then use GeoGebra to find:

::

   Root(0.1*x^2 - 1.5*x + 5)

Now let's ask sympy to do it:

::

   from sympy import *
   init_printing(use_unicode=True)
   x, y, z = symbols('x y z')
   p2 = 0.1*x**2 - 1.5*x + 5
   print(p2)
   solve(p2, x)
   Poly(p2, x).all_roots()





Third degree polynomials
========================

Start with this polynomial:

.. math::

   f(x) = x^3 - 3 x^2 - 4 x + 12

Input ``x**3 - 3*x**2 - 4*x + 12`` into geogebra.  Read the roots,
write it in factored form.


Fourth degree polynomials
=========================

.. math::

   p(x) = x^4 - 3 x^2 + 1

Plot it in geogebra with:

::

   x ** 4 - 3 * x ** 2 + 1

Then try manipulating it in sympy:

::

   from sympy import *
   init_printing(use_unicode=True)
   x, y, z = symbols('x y z')
   p4 = x ** 4 - 3 * x ** 2 + 1
   p4
   factor(p4)
   poly4 = Poly(p4, x)
   poly4.all_roots()
   p4 = x ** 4 - 3 * x ** 2 + 1.7
   factor(p4)

Fifth degree polynomials
========================

Let us craft a 5th degree polynomial.  In sympy let us write:

::

   from sympy import *
   init_printing(use_unicode=True)
   x, y, z = symbols('x y z')

   expr5 = (x - 1) * (x - 2) * (x + 1) * (x + 2) * (x - 7)
   expr5
   p5 = expand(expr5)
   p5
   print(p5)
   
