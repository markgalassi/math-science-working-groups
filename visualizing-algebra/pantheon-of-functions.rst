.. _chap-pantheon-of-functions:

***************************
 The pantheon of functions
***************************

Here we will take a broad tour of the types of functions that come up
in typical applications.

Broad categories
================

A list of categories with some simple examples.

polynomials
   .. math::
      f(x) = a_0 + a_1 x + a_2 x^2 + \dots + a_n x^n
rational functions
   .. math::

      f(x) = \frac{a_0 + a_1 x + a_2 x^2 + \dots + a_n x^n}
                  {b_0 + b_1 x + b_2 x^2 + \dots + b_k x^k}
algebraic functions
   .. math::

      f(x) = \sqrt[n]{\frac{a_0 + a_1 x + a_2 x^2 + \dots + a_i x^i}
                           {b_0 + b_1 x + b_2 x^2 + \dots + b_j x^j}}
elementary transcendental functions
   exp/log:
      :math:`e^x`, :math:`a^x`, :math:`\log(x)`, ...
   trig:
      :math:`\sin(\theta)`, :math:`\cos(\theta)`, :math:`\tan(\theta)`, ...
   inverse trig:
      arcsin(x), arccos(x), arctan(x)
   hyperbolic:
      :math:`\sinh(x)`, :math:`\cosh(x)`, :math:`\tanh(x)`, ...
   inverse hyperbolic:
      arcsinh(x), arccosh(x), arctanh(x)
special functions
   Gamma function :math:`\Gamma(x)`, Bessel functions :math:`J_n(x)`,
   Dirac delta function :math:`\delta(x)`, error function :math:`{\rm erf}(x)`
arithmetic functions (argument is an integer)
   Prime-counting function :math:`\pi(n)`

Polynomials
===========

We have already seen many examples of polynomials, so here we only
list a couple of plotting examples.  First for gnuplot locally on your
computer:

::

   # for gnuplot
   gnuplot
   # then at the gnuplot> prompt type:
   set grid
   set xrange [-3:3]
   plot x**3 - 3*x
   replot x**3 - 3*x**2 - 4*x + 12

The same formulas for a web calculator like GeoGebra or Desmos:

::

   x^3 - 3 x
   x^3 - 3 x^2 - 4 x + 12


Rational functions
==================

Rational functions are *ratios of polynomials*.  Let us start plotting
an example:

.. math::

   f(x) = \frac{x^2 - 4 x + 2}
               {x^3 - 3 x}

::

   gnuplot
   # then at the gnuplot> prompt type:
   reset
   set grid
   set samples 1000
   set yrange [-100:100]
   set xrange [-5:5]
   f(x) = (x**2 - 4*x + 2) / (x**3 - 3*x)
   plot f(x)

And in web graphical calculators:

::

   (x^2 - 4 x + 2) / (x^3 - 3 x)

Let us now unpack what we have been looking at.

First of all, the function is *not defined everywhere!* The
denominator is zero in three places :math:`(-\sqrt{3}, 0, \sqrt{3})`
which means that the overall function is not defined (or, as we often
say, it "blows up") at those three points.  This is clear in the graph.

The function also has *asymptotic behavior*, the behavior when
:math:`x` goes very far in the positive or negative direction (or, as
we often say, when :math:`x` goes to plus or minus infinity: :math:`x
\rightarrow \pm\infty`).

Asymptotic behavior is a matter of seeing whether the numerator or
denominator *dominates,* which depends on which has the higher power.

Now in class we experiment with rational functions that have higher,
lower, or equal powers in the numerator versus the denominator.

One way to do that is to change :math:`x^2` (in the numerator
polynomial) to :math:`x^3` and observe the asymptotic behavior.
Then go to :math:`x^4` and observe it again.

Another example would be:

.. math::

   f(x) = \frac{3x^3 - 5 x + 1}
               {-2x^3 + x + 1}

::

   gnuplot
   # then at the gnuplot> prompt type:
   reset
   set grid
   set samples 1000
   set yrange [-100:100]
   set xrange [-5:5]
   f(x) = (3*x**3 - 5*x + 1) / (-2*x**3 + x + 1)
   plot f(x)

And in web graphical calculators:

::

   (3 x^3 - 5 x + 1) / (-2 x^3 + x + 1)

I like to state a couple of take-home messages when I teach this
material:

#. The "close-by" behavior, around the origin, has two main features:
   *roots* (where we cross the x axis) and *singularities* (places where
   the function blows up).  The roots are determined by zeros in the
   numerator, while the singularities are determined by roots in the
   denominator.
#. The *asymptotic* behavior, as :math:`x \rightarrow \pm\infty`, is
   determined by ignoring all but the highest power of x, both above
   and below.  Our first fraction :math:`f(x) = \frac{x^2 - 4 x +
   2} {x^3 - 3 x}` then becomes:

   .. math::

      f(x) \approx \frac{x^2}{x^3}

   which converges to zero at both plus and minus infinity.  The
   second one :math:`f(x) = \frac{3x^3 - 5 x + 1}{-2x^3 + x + 1}`
   becomes:

   .. math::

      f(x) \approx \frac{3x^3}{-2x^3} = -\frac{3}{2}

   which converges to :math:`-3/2` in both directions.


Algebraic functions
===================

These can have roots in addition to all the rational function
components.

We can plot :math:`y = \sqrt{x}` and discuss its behavior, then moving
to :math:`\sqrt{x^3 - 3 x}`, just to see what they look like.  There
is no particular insight here, but it is worth mentioning that
:math:`\sqrt{x}` does grow to infinity, but it slows down a lot
compared to :math:`x`.

An interesting plot to examine next is:

.. math::

   y = \sqrt{3 + x^2}

After discussing what happens when x gets very big (i.e. you can
neglect the 3), you can then comment on how at :math:`x = 0` we seem
to have *curvy* behavior, but then the line seems to straighten out.
We can graph the following three separately:

::

   sqrt(3 + x^2)
   x
   -x

to see a neat effect.


Elementary transcendental functions
===================================

Start with a discussion of why they are called transcendental,
possibly mentioning what the distinction is between algebraic and
transcendental numbers.

Then get in to examples and pictures!

Exponentials and logarithms
---------------------------

Discussion of bases.

How fast to they grow?  Compare polynomial growth with exponential growth:

::

   gnuplot
   # then at the gnuplot prompt:
   reset
   set grid
   set xrange [0:4]
   plot 2**x
   plot x**2

in a web graphing calculator:

::

   2^x
   x^2

After exploring how these overtake each other, we add :math:`x^7` to
the plot and see how long it takes for the exponential to overtake
that.  We can also throw :math:`10^x` in for comparison.  Those two
polynomials and two exponentials should be enough to develop an
intuition for exponential versus polynomial growth.

Then we introduce the three bases: 2, 10, :math:`e`, and explain why
they all exist.

We then invert the exponential to get the logarithm function and we
draw it.

One use for logarithms is to compress the axis in a plot.  To motivate
the use of log in plots:

What if you have something that grows fast.  Look at internet sites:

https://web.archive.org/web/20110614121350/http://www.isc.org/solutions/survey/history

https://www.isc.org/survey/

https://en.wikipedia.org/wiki/List_of_sovereign_states_by_number_of_Internet_hosts

and possibly from Our World in Data.

Also look at history of human population.


trigonometric functions
=======================

A detour to talk about *radians* versus *degrees*.  This is covered in
:numref:`sec-getting-comfortable-with-radians`.

We first learn to plot sin, cos, and tan.  The others are not really
important.  We plot them over several periods.

Then we discuss frequency, period, amplitude.

We then conclude with the resemblance that parts of sin and cos have
with polynomials, and mention that in the future we will learn about
Taylor series.

This discussion of approximation by polynomials allows us to point out
that radians are the more natural unit of measure for angles.



Special functions
=================

Gamma function
--------------

.. math::

   \Gamma(n) = (n-1)!

The full definition of the gamma function for all real (and in fact
complex) numbers is more advanced.  I show it here, but it is a
subject for much later on:

.. math::

   \Gamma(z) = \int_0^\infty x^{z-1} e^{-x} dx

Now discuss gamma function (and factorial) growth compared to
polynomial and exponential.  Refer to
:numref:`sec-a-digression-on-the-factorial`

Other special functions
-----------------------

Unfortunately it is hard to demonstrate these in the web graphing
calculators, so we will leave it out for now.
