.. _chap-introducing-symbolic-algebra:

******************************
 Introducing symbolic algebra
******************************

Wouldn't it be nice...
======================

You might have struggled with handling some long gnarly mathematical
expressions, and you might have wondered either:


Wouldn't it be nice if they just gave me the numbers, so I can punch
them into a calculator, and I don't have to figure out the *general*
answer with all the :math:`x` and :math:`y` variables...

For example, if my boss asks me "find all the values of x for which
this equation is true":

.. math::

   & x^{5} + x^{4} \left(- 3 \pi - \sqrt{7} + 0.4\right) + x^{3}
   \left(-12.8 - 1.2 \pi - 0.4 \sqrt{7} + 3 \sqrt{7} \pi\right) \\
   & + x^{2} \left(-19.2 + 1.2 \sqrt{7} \pi 12.8 \sqrt{7} + 38.4
   \pi\right) \\
   & + x \left(- 38.4 \sqrt{7} \pi + 19.2 \sqrt{7} + 57.6 \pi\right) -
     57.6 \sqrt{7} \pi \\
   & = 0

I wish I could just say: "sure, boss, the solutions are
:math:`(2.64575131107, -2, 4, -2.4, 9.42477796077)`"

Or wouldn't it be nice if there were a computer program which could do
all those algebra or calculus manipulations for me *preserving* the
variables without plugging numbers in -- all those messy expressions
they had me do in algebra class...

For example, if my boss asked me: "find me the derivative of
:math:`\sin(7 x^3) e^{-x^2}`", I wish I could just say (with no
effort):

.. math::

   \frac{d \left(\sin(7 x^3) e^{-x^2}\right)}{dx}
   = 21 x^{2} e^{- x^{2}} \cos{\left(7 x^{3} \right)}
   - 2 x e^{- x^{2}} \sin{\left(7 x^{3} \right)}

The first of these wishes is the subject of **numerical analysis**,
the second is the subject of **symbolic math**.

In this working group we will be working with a lot of algebraic
expressions, and one way in which we will enhance this beyond what is
done in school will be to use symbolic math.

We will conclude every segment with an interactive use of the *sympy*
symbolic algebra system to have the computer redo our hard-work
calculations.


Preparing your computer to use sympy
====================================

On the web
----------

I recommend that at some point you learn to use sympy on your computer
with a full python environment.  But for the purpose of this course we
will use the public web installation called *sympygamma* --
https://sympygamma.com/ and enter expressions there.

More complex expressions, but with more prep required, can be used at
https://live.sympy.org/ - this will look more like what you do on a
computer, showing you the python instructions they use to start.


On a desktop or laptop computer
-------------------------------

On a debian-based linux distribution you can run:

.. code-block:: console

   $ sudo apt install python3-sympy

Alternatively, on just about any computer you can run:

.. code-block:: console

   $ pip3 install sympy

You should now be able to type ``python3`` and then in the python
interpreter:

.. code-block:: pycon

   >>> import sympy
   >>> from sympy import *
   >>> init_printing()
   >>> x, y, z, t = symbols('x y z t')
   >>> k, m, n = symbols('k m n', integer=True)
   >>> f, g, h = symbols('f g h', cls=Function)

This will allow you to use :math:`x, y, z, t, k, m, n, f, g, h` as
symbolic variables.


Getting started with a tutorial
===============================

We will work with the simpler *sympygamma*, which has a collection of
example expressions.  They are well chosen, so we will work through
them, discussing what it has done after we do each expression.

First we can warm up by putting in simple arithmetic, but we won't do
more than one or two expressions, like:

::

   22 / 7
   355 / 113

Then we will expand some expressions:

::

   (a + b)**2
   expand((a + b)**2)
   expand((a + b)**3)
   expand((a + b)**4)
   expand((a + b)**5)
   expand((a + b)**6)
   expand((a + b)**7)
   expand((a + b)**8)

Then let us follow their algebra examples:

::

    x
    (x+2)/((x+3)(x-4))
    simplify((x**2 - 4)/((x+3)(x-2)))

If you are curious about how that expression simplified one you can
factor :math:`x^2 - 4` in your head, or you can put into sympy:

::

   factor(x**2 - 4)

Now some polynomial and rational functions.  First put the raw
polynomials that will come up into desmos:

::

   x^4 / 2 + 5 x^3 / 12 - x^2 / 3
   x^2 + 4 x y + 4 y^2 = 0
   x^2 + 4 x + 181
   x^3 + 4 x + 181

(Note that the last two have that big y offset, so you will need to
hunt for them in the graph.)

And then the expressions to put in to sympygamma:

::

    div(x**2 - 4 + x, x-2)
    # my comment for div(): polynomial division with remainders is
    # not interesting
    gcd(2*x**2 + 6*x, 12*x)
    lcm(2*x**2 + 6*x, 12*x)
    # my comment for gcd() and lcm(): this mostly goes to show that
    # polynomials have a lot in common with integers - I make a brief
    # passing mention of "rings" as an algebraic structure.
    factor(x**4/2 + 5*x**3/12 - x**2/3)
    factor(x**2 + 4*x*y + 4*y**2)
    solve(x**2 + 4*x*y + 4*y**2)
    solve(x**2 + 4*x*y + 4*y**2, y)
    solve(x**2 + 4*x + 181, x)
    solve(x**3 + 4*x + 181, x)
    solve_poly_system([y**2 - x**3 + 1, y*x], x, y)

That last expression corresponds to the system of polynomial
equations:

.. math::
   :nowrap:

   \begin{eqnarray}
   \begin{cases}
   y^2 - x^3 + 1 & = 0 \cr
   x y & = 0 \cr
   \end{cases}
   \end{eqnarray}


The sympy system is well documented.  You can start from
https://sympy.org/ and follow their documentation link to reach the
tutorial page at
https://docs.sympy.org/latest/tutorials/index.html#tutorials

The class now moves jumps away from this chapter to their tutorial,
starting with the examples at:

https://docs.sympy.org/latest/tutorials/intro-tutorial/intro.html

and continue to their examples of simplification and factoring:

https://docs.sympy.org/latest/tutorials/intro-tutorial/simplification.html

We can stop now, since we will return to sympy examples as we cover
those topics.

[unsorted] Some expressions we will do
======================================

#. Simple systems of two linear equations with two unknows, starting
   with two equations in the form :math:`y = m x + b`, rewriting them
   as :math:`m x - y + b = 0`, and then putting them into sympy.  Have
   the students pick all the coefficients.  For example:

   .. math::
      :nowrap:

      \begin{eqnarray}
      \begin{cases}
      y \; = & \frac{1}{2} x + 4.2 \cr
      y \; = & - 3 x - \frac{1}{2} \cr
      \end{cases}
      \end{eqnarray}

#. System of a linear equation and a quadratic equation.  Have the
   students pick all the coefficients.  You could keep one of the
   lines from before.  For example:

   .. math::
      :nowrap:

      \begin{eqnarray}
      \begin{cases}
      y \; = & \frac{1}{3} x^2 - 2 x - 1 \cr
      y \; = & \frac{1}{2} x + 4.2 \cr
      \end{cases}
      \end{eqnarray}

#. The famous gaussian integral:

   .. math::

      \int_{-\infty}^\infty e^{-x^2} dx


::

   solve([2*x+a*y-z-1, -7*x+y+2*z+1, x-b*y-z+3], x, y, z)
