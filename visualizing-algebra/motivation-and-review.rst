.. _chap-visualizing-algebra-motivation:

*************************************************************
 Visualizing algebra: motivation and review of prerequisites
*************************************************************

.. _sec-visualizing-algebra-motivation-and-plan:

Motivation and plan
===================

Purpose:
   The goal for this working group on *visualizing algebra* is to build:

   *Agility and wide experience in visualizing and solving equations.*

In-sequence:
   This fits in the broad sequence of skills that you might call
   "practical math for research".
Computer use:
   We will use *plotting programs* and *plotting web sites* to draw
   functions and data.  But:
Programming:
   Almost no programming.  Some explorations can include canned
   python programs which you can execute without knowing the language.
Examples from:
   Social science, news and current events, science, sports, ...
Auxiliary textbooks:
   `OpenSTAX textbook on Algebra and
   Trigonometry
   <https://openstax.org/details/books/algebra-and-trigonometry-2e>`_
   :cite:p:`AbramsonJay2021AaT2`.
Other interesting books to accompany this material:
   *"No Bullshit Guide to Math and Physics"* by Ivan Savov
   :cite:p:`savov2014no`.
Who should join?
   This does not fit in school curriculum boundaries, but if I had to
   narrow it: the material is aimed at students who are taking the US
   courses Algebra 1, Geometry, and Algebra 2.  It supplements and
   makes real what students will learn in those courses, and might
   give them the feeling of excitement that the courses sometimes do
   not provide.

   Users taking more advanced courses, like the US pre-caluculus and
   calculus, might find very useful review and some new material here
   (approaches to visualization, symbolic algebra, ...).  But if they
   do not need review then most of it will be old-hat and maybe not
   the best use of their time.

   Those more advanced students might be interested in the other math
   working group we have: "Math for Research", discussed at this link:
   :ref:`chap-math-for-research-motivation`.
Are there other things we get out of this?
   A "working group" is very different from a classroom setting: you
   can ask conceptual questions, advanced questions, super-basic
   questions, questions that revisit the things you learned way back,
   and have a discussion with the instructor and other students.
   Semi-formal conversations with an experienced mathematician can be
   a new and useful experience.


Now let us start by reviewing some of the prerequisite materials,
before we start with straight lines and then move on to polynomials.

Information on signing up for the next working group should be in the
chapter :ref:`chap_logistics-for-specific-courses`.


.. _sec-visualizing-algebra-review-of-prerequisites:

Review of prerequisites
=======================

In this section we will look at some basic material which we have seen
before, but probably not in the context of producing visualizations
and other practical results that we want to see.

We plan to make quick work of these topics, but we will still treat
them thoughtfully.  For each topic we will:

.. hlist::
   :columns: 1

   * look at the definition, explanation, and exercises in the
     textbook
   * do some visualization and application to an area of interest

.. note::

   This might be the most important chapter in the course: it turns
   out that there is a lot of diversity in what people have studied so
   far, and *everyone* appreciates the review.  Another important part
   of the review is that we discuss the *reasons* for formulas like
   :math:`a^0`, :math:`x^{-3}`, and :math:`y^\frac{1}{2}`, including
   putting it in the context of how we had have had to introduce new
   mathematical structures throughout our life (naturals, integers,
   rationals, reals, complex, ...)  Some students really resonate with
   that approach.

The "why" of messy exponents
----------------------------

This will be an interactive discussion of why we have expressions like
:math:`a^0`, :math:`b^{-3}`, :math:`x^\frac{1}{2}`.

A handy table for once we have understood them:

.. math::
   :nowrap:

   \begin{eqnarray}
   a^0 & = 1 \;\;\;\;   & \textrm{for any a != 0} \cr
   b^{-3} & = \frac{1}{b^3} \;\;\;\; & \textrm{for any b != 0} \cr
   x^{\frac{1}{2}} & = \sqrt[2]{x} & \textrm{for any x} \cr
   x^{\frac{m}{n}} & = \sqrt[n]{x^m} & \textrm{for any x} \cr
   \end{eqnarray}

In class I give a lengthy explanation of why we introduce these messy
exponents:

#. Write down the definition of powers, as we learned them in 6th
   grader or so, as a *typographical* definition (this is my own use
   of the word, losely based on Douglas Hofstadter's "Typographical
   Number Theory"): you simply define :math:`3^4` as "write down the
   number 3 four times, and put multiplication signs in between":

   .. math::

      3 \times 3 \times 3 \times 3
#. Emphasize with lots of gesticulation that this only defines
   :math:`b^n` *if n is a positive integer!!!!*
#. Then play around with those rules of adding exponents and show
   examples.  (The students usually need to be reminded of this.)
#. Then show how you can subtract exponents and get the division of
   powers of the same base, but emphasize with great emphasis that
   *this only works if the first exponent is strictly greater than the
   second one!!!!* otherwise it doesn't mean anything.

   .. math::

      \frac{b^n}{b^k} = b^{n-k} \;\;\; \textrm{ONLY IF} \;\;\; n > k
#. Now we imagine what would happen if k could be bigger than n, and
   we ask "if we extend the definition of subtracting exponents to
   allow a negative power, do we get a consistent arithmetic?  Since
   we do, we decide to use the notation *convention* of

   .. math::

      {b^{-k}} = \frac{1}{b^k}

   We note that it does not follow from the definition - it's a
   notation convention.

We then do similar reasoning for :math:`a^0`, :math:`x^{1/2}` and
:math:`x^{m/n}`

A nostalgic romp through coordinates and plotting
-------------------------------------------------

Quickly read the `Linear Equations in One Variable
<https://openstax.org/books/algebra-and-trigonometry-2e/pages/2-2-linear-equations-in-one-variable>`_
chapter in :cite:p:`AbramsonJay2021AaT2`.

Exercises: 2, 4, 6, 8, 10 in `Review Exercises
<https://openstax.org/books/algebra-and-trigonometry-2e/pages/2-review-exercises>`_


What are functions?
-------------------

https://openstax.org/books/algebra-and-trigonometry-2e/pages/3-introduction-to-functions

It is useful to point out that functions have a very general meaning,
even though we will mostly work with functions of real numbers.  So
start by reading:

Quickly read the `Functions and Function Notation <https://openstax.org/books/algebra-and-trigonometry-2e/pages/3-1-functions-and-function-notation>`_
chapter in the OpenStax algebra book :cite:p:`AbramsonJay2021AaT2`.

and work the examples and "try it" exercises given in the rubrics from
"EXAMPLE 5" all the way to "TRY IT #6".

Special powers of binomials
---------------------------

It is crucial to review what these look like: it is taught in schools,
but somehow most students miss it and reach high school not knowing
these expresions.  You can use our glossary to remember what
:term:`monomials` and :term:`binomials` are.  Let us look at these
expressions:

.. math::

   (a + b)^2 = a^2 + 2ab + b^2 \\
   (a - b)^2 = a^2 - 2ab + b^2 \\
   (a + b)(a-b) = a^2 - b^2

There are many other such basic identities, but these are the ones I
always look at.

We will do two things with this: the first is to actually work them
out and see why they are correct.  Everyone has their way of doing
this - I put my two index fingers down and scan each expression until
I have multiplied everything out.

The second thing we will do is to get used to :term:`factoring`
polynomials using those identities.  To do so it's useful to write
them flipping the left and right hand side of the ``=`` sign:

.. math::

   a^2 + 2ab + b^2 = (a + b)^2 \\
   a^2 - 2ab + b^2 = (a - b)^2 \\
   a^2 - b^2 = (a + b)(a-b)


One approach to factoring polynomials is a "visual inspection", where
your brain does pattern-matching on things that "look like a perfect
square".  Here are some examples: we will look at them together and
discuss how we found the factors.

.. math::
   :nowrap:

   \begin{eqnarray}
   x^2 - 4 = & (x + 2) (x - 2) & \\
   x^2 - 3 = & \; (x + \sqrt{3}) (x - \sqrt{3}) & \\
   25 x^2 + 20 x + 4 = & (5x + 2) (5x + 2) & = (5x + 2)^2 \\
   49 x^2 - 14 x + 1 = & (7x - 1) (7x - 1) & = (7x - 1)^2
   \end{eqnarray}

The first one is the simple recognition that you have a difference of
perfect squares.  The second one shows that the technique is useful
even if the *number* is not a perfect square.

The third involves recognizing that both the coefficient of
:math:`x^2` (:math:`5^2`) and the constant term 4 (:math:`2^2`) are
perfect squares, and the coefficient of :math:`x` looks like the
"double product" :math:`2 \times 5 \times 2 = 20`.

The fourth again involves recognizing that both the coefficient of
:math:`x^2` (:math:`7^2`) and the constant term 1 (= :math:`1^2`)
are perfect squares, and the coefficient of :math:`x` looks like the
"*negative* double product" :math:`2 \times 7 \times (-1) = -14`.


More discussion, examples, and exercises on factoring polynomials is
in the chapter `Factoring Polynomials
<https://openstax.org/books/algebra-and-trigonometry-2e/pages/1-5-factoring-polynomials>`_
in the OpenStax algebra book :cite:p:`AbramsonJay2021AaT2`, which also
discusses more elaborate factoring techniques than this simple visual
inspection.

I recommend becoming quite comfortable with factoring polynomials: it
comes up a lot in the life of a researcher.  The "visual inspection"
approach should is one you should always know, while the more
algorithmic technique shown in the book is one that you should
understand and be ready to look up.

Finally: try factoring these same polynomials using SymPy!  You can
put these expressions into the SymPy Gamma web calculator:

::

   factor(25* x**2 + 20 *x + 4)
   factor(x**2 - 3)
   factor(49*x**2 - 14*x + 1)

As a final note on the :math:`(a + b)^2` expansion, it's good to
remember the classic "Pascal triangle":

.. code-block:: text

   0             1
   1            1 1
   2           1 2 1
   3          1 3 3 1
   4         1 4 6 4 1
   5       1 5 10 10 5 1

which allows us to automatically write down, with no effort:

.. math::

   (a + b)^0 & = 1 \\
   (a + b)^1 & = a + b \\
   (a + b)^2 & = a^2 + 2ab + b^2 \\
   (a + b)^3 & = a^3 + 3a^2b + 3ab^2 + b^3 \\
   (a + b)^4 & = a^4 + 4a^3b + 6a^2b^2 + 4ab^3 + b^4 \\
   (a + b)^5 & = a^5 + 5a^4b + 10a^3b^2 + 10a^2b^3 +5ab^4 + b^5

One fun thing to note is that the sum of the exponents of a and b in
each term is always the power of the binomial we are expanding.

For example, :math:`(a+b)^4` has terms like :math:`6a^2b^2` and
:math:`4ab^3`, where 2+2 and 1+3 are both 4.

This relates slightly to ideas in physics: if a and b had physical
dimensions (like length or mass) you would get inconsistent
expressions if the exponents did not add up to the same amount -- you
cannot add an area to a volume, for example!

Reviewing fractions -- what, seriously??
----------------------------------------

This is another topic where student preparation is uneven, and it is a
good chance to show some tricks.

Simplifying fractions
^^^^^^^^^^^^^^^^^^^^^

Start with talking through simplifying these fractions:

* :math:`\frac{36}{3}`
* :math:`\frac{128}{1024}`
* :math:`\frac{512}{384}`

A useful tool to have handy is the "prime factorization".  You can
simply do a web search for "prime factors 384" and you will get
:math:`2^7 \times 3`

If you are running linux you can go faster with the command line
program ``factor``:

.. code-block:: console

   $ factor 512
   2 2 2 2 2 2 2 2 2
   $ factor 384
   2 2 2 2 2 2 2 3

This tells us that:

.. math::

   \frac{512}{384} = \frac{2^9}{2^7 \times 3}
   = \frac{2^7 \times 2^2}{2^7 \times 3} 
   = \frac{\cancel{2^7} \times 2^2}{\cancel{2^7} \times 3} = \frac{4}{3}

These kinds of simplifications are very useful, and they become even
more important when we *multiply* fractions.

Important mantra vis-a-vis fractions:

.. note::

   Multiplying and dividing fractions is easy.  Adding and subtracting
   fractions is annoying.

Multiplying (and dividing) fractions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Multiplying fractions involves just multiplying the top and bottom:

.. math::

   \frac{a}{b} \times \frac{c}{d} = \frac{a \times c}{b \times d}

This is easy!  Just what you would dream of.  (With plus and minus our
dream will be crushed...)

So you can do simple things like:

.. math::

   \frac{2}{5} \times \frac{3}{7} = \frac{6}{35}

But sometimes the numbers get pretty big, and this is where you want
to *simplify before you multiply*.  Example:

.. math::

   \frac{2}{5} \times \frac{30}{7}
   = \frac{2}{5} \times \frac{6 \times 5}{7}
   = \frac{2}{\cancel{5} 1} \times \frac{6 \times \cancel{5} 1}{7}
   = \frac{2}{1} \times \frac{6}{7} = \frac{12}{7}

Let us do together:

* :math:`\frac{12}{5} \times \frac{20}{27}`
* :math:`\frac{1}{3} \times \frac{18}{19}`
* :math:`\frac{49}{3} \times \frac{12}{21}`

Dividing fractions is just one step from multiplying them.  You flip
the second (which is called taking the *reciprocal*) and then multiply
them:

.. math::

   \frac{a}{b} \div \frac{x}{y} = \frac{a}{b} \times \frac{y}{x}

Note that I will be trying to move students away from ever using the
:math:`\div` symbol, in favor of always using fractions.  So you might
end up with this kind of situation:

.. math::

   \frac{\ \frac{a}{b}\ }{\ \frac{x}{y}\ } = \frac{a}{b} \times \frac{y}{x}


Adding (and subtracting) fractions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Adding (and subtracting) fractions is annoying.  The famous
"freshman's dream" is incorrect:

.. math::

   \cancel{\frac{a}{b} + \frac{c}{d} = \frac{a + c}{b + d}} \ \
   \textrm{WRONG/VIETATO/VERBOTEN !!!!}

and yet many kids write it down thinking it must be true, because life
should occasionally give us something that simple.

Instead you have to do the procedure we learned way back of making a
*common denominator*.

It's worth the effort of doing a couple of examples of common
denominators with the students, and then quickly moving on.


Getting comfortable with visualizing functions
----------------------------------------------

We have two angles here: one is to become comfortable with what
various functions look like.  The other is to look at how the
intersection (simultaneous solution) of functions in higher dimensions
gives insight into the solutions to equations.

The functions most of our students will have seen are linear functions
and quadratic functions, so we will visualize a few of these.

In geogebra or desmos enter the following *straight line* functions of
x, and discuss the slope and intercepts:

.. code-block:: text

   (1/2) x + 3
   x - 2
   -2 x + 4
   -x - 1

Now insert the following quadratic functions:

.. code-block:: text

   x^2
   x^2 - 4
   -x^2
   -x^2 - 4
   -x^2 - x
   -x^2 - x - 1
   -x^2 - x + 4

and discuss issues like how many roots there are, and whether some of
those roots are complex.

More on this topic, which will not be review for most people, will be
in :numref:`chap-visualizing-functions`.


Quadratic equations
-------------------

See the OpenStax chapter on
`Quadratic equations
<https://openstax.org/books/algebra-and-trigonometry-2e/pages/2-5-quadratic-equations>`_

This will give us several examples and "TRY IT" exercises that we can
do by inspection.  For example, we can do example 1, TRY IT 1, example
2, TRY IT 2, example 3, TRY IT 3.

As we do each of these exercises we also plot them in our online
graphing calculator.

Motivated students could also go on to the "completing the square"
section, but in the working group I would recommend skipping it and
moving on.


Some heavy emphasis on how functions are *constraints*
------------------------------------------------------

.. admonition:: The crucial nexus

   There is a crucial point that everyone has to get used to, so we
   will bring out our graphing calculator and keep talking about it
   until either everyone falls asleep, or everyone gets it: what does
   it mean to translate the function to a plot?

We will use the words *satisfy* and *constrain* quite a bit, and we
will keep using them until people get comfortable.

The phrases we use to drive home how a function defines the set of
:math:`(x, y)` points will be phrases like these:

"When I write :math:`y = 1.2 x - 1`, I am defining the collection of
all points :math:`(x, y)` for which that equal sign *is true*"

"When I write :math:`y = 1.2 x - 1`, I am defining the collection of
all points :math:`(x, y)` that *satisfy that relationship*."

"When I write :math:`y = 1.2 x - 1`, I am defining the collection of
all points :math:`(x, y)` that *satisfy the constraint* of that
equation.

"When I write :math:`y = 1.2 x - 1`, I am restricting the possible
:math:`(x, y)` quite dramatically.  Before I wrote the function, the
entire 2-dimensional plane was 'fair game', but now only a single line
is part of that."

"When I write :math:`y = f(x)`, I have reduced the *dimensionality* of
the space from the 2-dimensional Euclidean plane to a 1-dimensional
curve that lives in that plane."

Let's draw a lot of these with our online graphing calculator, and
talk about dimensionality.

Finally, let us talk about what it means to satisfy *two* equations at
the same time:

Put these two equations in your graphing program:

.. math::
   :nowrap:

   \begin{eqnarray}
   \begin{cases}
   y & = x^3 - 1 \cr
   y & = 2 x + 1 \cr
   \end{cases}
   \end{eqnarray}

Then subtract a bit from the second one to have something like
:math:`y = 2 x + 1 - 2`.

Here we talk about how having *two* constraints reduces the dimension
of the space from *2* to *0* (a point has dimension 0; a discrete
collection of points also has dimension 0).

Here we can gab a bit about a detective drama where the pool of
suspects is cut dramatically when you talk about the color of their
eyes, or if they are left-handed, ...

We will talk more about the dimensions of spaces and subspaces when we
solve more complex equations and systems of equations.


Two equations with two variables
--------------------------------

This is probably still "review" for most students, so we take a brief
look at *systems of two linear equations with two unknowns*.

We will look at the OpenStax chapter
`Systems of Linear Equations: Two Variables
<https://openstax.org/books/algebra-and-trigonometry-2e/pages/11-1-systems-of-linear-equations-two-variables>`_

Our approach here will be to:

#. Graph some pairs of equations like those in Figure 2 in the
   OpenStax book and discuss visually when solutions exist and when
   they do not.  We will then make similar graphs in geogebra or
   desmos for each system we look at.
#. Look at some simple exercises that can be solved either *by
   inspection* or with a small amount of calculation.
#. Remember the two often-used approaches: *substitution* and
   *gaussian elimination*.

With this in mind, let us do example 1 through TRY IT #5.
