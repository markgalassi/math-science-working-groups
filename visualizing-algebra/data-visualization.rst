.. _chap-data-visualization:

********************
 Data visualization
********************

We have focused mostly on visualizing *functions*, but it is important
to also know how to visualize *data*.

Since it is not a part of this algebra course I will simply give a
pointer to where you can find a lesson I wrote on it for the "Research
Skills and Critical Thinking" book:

https://markgalassi.codeberg.page/research-skills-html/data-visualization.html
