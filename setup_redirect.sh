#! /bin/sh

# only needed for October/November 2023 as I advertised some links
# that are changing, so I need html pages to redirect.

exit 1

for fname in _build/html/visualizing-algebra/*.html
do
    redirect_fname=`echo $fname | sed 's/visualizing-algebra/analytic-geometry/'`
    echo "redirect: $redirect_fname"
    mkdir -p `dirname $redirect_fname`
    (echo '<!DOCTYPE html><html><head><body><p>This page has moved to: <a href="' \
     && echo ../visualizing-algebra/`basename $fname` \
     && echo '">this new location</a></p></body></html>') > $redirect_fname
done
