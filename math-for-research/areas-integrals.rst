***********************************
 Approximating areas and integrals
***********************************

Motivation and plan
===================

The "area under a curve" is the general 2-dimensional area problem.

We know how to calculate the area of disks, spherical surfaces,
cilinders, trapezoides, and many other shapes.  But the problems that
come up in real work do not have exact solutions.
