#! /usr/bin/gnuplot
set samples 1000
plot [] [-1.3:1.3] sgn(sin(x)) lw 2, \
 (4.0/pi) * sin(x), \
 (4.0/pi) * (1.0/3)*sin(3*x), \
 (4.0/pi) * (1.0/5)*sin(5*x), \
 (4.0/pi) * sin(x) + (1.0/3)*sin(3*x) + (1.0/5)*sin(5*x) lw 2
# ## now look at the summed-up plot by itself:
# plot (4.0/pi) * (sin(x) + (1.0/3)*sin(3*x) + (1.0/5)*sin(5*x)) t '5x'
