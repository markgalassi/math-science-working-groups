#! /usr/bin/env python3

import sys
import numpy as np
from scipy import fftpack
from matplotlib import pyplot as plt

np.random.seed(1234)

# N = 1000
# tmax = 20
# time_step = (tmax - 0) / N
# time_vec = np.arange(0, tmax, time_step)
# period = 5.0
# base_freq = 1 / period
# a sum of sin waves with frequencies 1 and 2 and 3 times the base
# frequency (cycles/sec)
# sig = (np.sin(2 * np.pi / period * time_vec)
#        + 0.4 * np.random.randn(time_vec.size))

def main():
    # grab the signal from the file
    fname = 'tuning-fork.dat'
    do_batch = False
    if len(sys.argv) == 2:
        fname = sys.argv[1]   # they can override the file name
    elif len(sys.argv) == 3:
        assert(sys.argv[1] == '--batch')
        do_batch = True
        fname = sys.argv[2]   # they can override the file name
    else:
        sys.stderr.write(f'usage: {sys.argv[0]} [--batch] file.dat\n')
    # fname = 'white-noise-small-sample.dat'
    time_vec, sig = load_file_columns(fname, 0, 1) # columns 0 and 1
    assert(len(time_vec) == len(sig))
    N = len(time_vec)
    time_step = time_vec[1] - time_vec[0]

    # plot the original signal
    plot_original(time_vec, sig, 311, 'original')

    # The FFT of the signal
    sig_fft = fftpack.fft(sig)
    # The corresponding frequencies
    freqs = fftpack.fftfreq(len(sig), d=time_step)
    plot_fft(freqs[:N//2], np.abs(sig_fft[:N//2]), 312, 'fft')

    # And the power (sig_fft is of complex dtype)
    power = np.abs(sig_fft)**2

    # plot the fft, zoomed in
    plot_fft(freqs[:N//8], np.abs(sig_fft[:N//8]), 313, 'xzoom')

    plt.legend(loc='best')
    # save the files for the book
    for suffix in ['png', 'svg']:
        ofname = f'fft_{fname[:-4]}.{suffix}'
        print(f'saving file {ofname}')
        plt.savefig(ofname)
    if not do_batch:
        plt.show()


def plot_original(times, sig, subp, ylab):
    plt.subplot(subp)
    plt.ylabel(ylab)
    plt.plot(times, sig, label=ylab)


def plot_fft(freqs, sigfft, subp, ylab):
    plt.subplot(subp)
    plt.ylabel(ylab)
    markerline, stemlines, baseline = plt.stem(freqs, np.abs(sigfft), '-.')
    plt.setp(stemlines, 'linewidth', 0.2)
    # plt.stem(freqs, np.abs(sigfft))

def filter_low_pass(freqs, sigfft, cutoff):
    high_freq_fft = sigfft.copy()
    # zero out the fft for all frequences higher than the cutoff.
    # this uses numpy's clever mechanism for using a condition to get
    # an array subset.  for example:
    # In [8]: a = np.array([1,2,3,4,5,6,7,8])
    # In [11]: a % 2 == 0
    # Out[11]: array([False,  True, False,  True, False,  True, False,  True])
    # In [12]: a[a % 2 == 0]
    # Out[12]: array([2, 4, 6, 8])
    high_freq_fft[np.abs(freqs) > cutoff] = 0
    filtered_sig = fftpack.ifft(high_freq_fft)
    return high_freq_fft, filtered_sig


def filter_high_pass(freqs, sigfft, cutoff):
    high_freq_fft = sigfft.copy()
    # zero out the fft for all frequences higher than the cutoff.
    # this uses numpy's clever mechanism for using a condition to get
    # an array subset.  for example:
    # In [8]: a = np.array([1,2,3,4,5,6,7,8])
    # In [11]: a % 2 == 0
    # Out[11]: array([False,  True, False,  True, False,  True, False,  True])
    # In [12]: a[a % 2 == 0]
    # Out[12]: array([2, 4, 6, 8])
    high_freq_fft[np.abs(freqs) < cutoff] = 0
    filtered_sig = fftpack.ifft(high_freq_fft)
    return high_freq_fft, filtered_sig


def load_file_columns(fname, col0, col1):
    """Read two columns from an ascii file."""
    times = []
    signal = []
    with open(fname, 'r') as f:
        for line in f.readlines():
            if line[0] == ';':
                continue        # skip comments
            words = line.split()
            w0 = float(words[col0])
            times.append(w0)
            w1 = float(words[col1])
            signal.append(w1)
    return times, signal


if __name__ == '__main__':
    main()
