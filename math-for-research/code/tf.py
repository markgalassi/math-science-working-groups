#! /usr/bin/env python3
import numpy as np
from scipy.fftpack import fft

# Number of sample points
N = 600
dt = 1.0 / 800.0
x = dt*np.arange(N)
y = np.sin(50.0 * 2.0*np.pi*x) + 0.5*np.sin(80.0 * 2.0*np.pi*x)
yf = fft(y)
xf = 1/(N*dt)*np.arange(N//2)
import matplotlib.pyplot as plt
plt.plot(xf, 2.0/N * np.abs(yf[0:N//2]))
plt.grid()
plt.show()
