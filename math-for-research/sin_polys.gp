#! /usr/bin/gnuplot
set grid
plot [-6:6] [-1.5:1.5] sin(x) lw 3, x, x - x**3 / 3!,  x - x**3 / 3! + x**5 / 5!,  x - x**3 / 3! + x**5 / 5! - x**7 / 7!,  x - x**3 / 3! + x**5 / 5! - x**7 / 7! + x**9 / 9!
