#! /usr/bin/gnuplot
set grid
plot [-6.3:6.3] [-1.5:1.5] cos(x) lw 3,  1 - x**2 / 2!,  1 - x**2 / 2! + x**4 / 4!,  1 - x**2 / 2! + x**4 / 4! - x**6 / 6!,  1 - x**2 / 2! + x**4 / 4! - x**6 / 6! + x**8 / 8!,  1 - x**2 / 2! + x**4 / 4! - x**6 / 6! + x**8 / 8! - x**10 / 10!
