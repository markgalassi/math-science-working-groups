#! /bin/sh

wget --continue https://upload.wikimedia.org/wikipedia/commons/6/66/Pendulum_gravity.svg
convert Pendulum_gravity.svg Pendulum_gravity.png
wget --continue https://upload.wikimedia.org/wikipedia/commons/6/6b/SPI_timing_diagram2.svg
convert SPI_timing_diagram2.svg SPI_timing_diagram2.png

# for series.rst
gnuplot -e "set terminal pdf" -e "set output 'sin_polys.pdf'" sin_polys.gp
gnuplot -e "set terminal png" -e "set output 'sin_polys.png'" sin_polys.gp
gnuplot -e "set terminal pdf" -e "set output 'cos_polys.pdf'" cos_polys.gp
gnuplot -e "set terminal png" -e "set output 'cos_polys.png'" cos_polys.gp
gnuplot -e "set terminal pdf" -e "set output 'rational_polys.pdf'" rational_polys.gp
gnuplot -e "set terminal png" -e "set output 'rational_polys.png'" rational_polys.gp
gnuplot -e "set terminal pdf" -e "set output 'lorenz_factor.pdf'" lorenz_factor.gp
gnuplot -e "set terminal png" -e "set output 'lorenz_factor.png'" lorenz_factor.gp

# for fourier-analysis.rst
gnuplot -e "set terminal pdf" -e "set output 'fourier_square.pdf'" fourier_square.gp
gnuplot -e "set terminal png" -e "set output 'fourier_square.png'" fourier_square.gp

gnuplot -e "set terminal pdf lw 3" -e "set output 'gaussian.pdf'" gaussian.gp
gnuplot -e "set terminal png lw 3" -e "set output 'gaussian.png'" gaussian.gp

# simple filtering demonstration
./code/filter_toy.py &
./code/filter_random.py &

# for music

# tuning fork
wget https://freesound.org/data/previews/126/126352_2219070-lq.mp3 -O tuning-fork.mp3
ffmpeg -y -i tuning-fork.mp3 tuning-fork.aif
sox tuning-fork.aif tuning-fork.dat
gnuplot -e "set terminal png" -e "set output 'tuning-fork.png'" -e "plot 'tuning-fork.dat' using 1:2 with lines"
python3 code/fft_audio.py --batch tuning-fork.dat

# white noise
wget -q --continue http://www.vibrationdata.com/white1.mp3 -O white-noise.mp3
ffmpeg -y -i white-noise.mp3 white-noise.aif
sox white-noise.aif white-noise.dat
gnuplot -e "set terminal png" -e "set output 'white-noise.png'" -e "plot 'white-noise.dat' using 1:2 with lines"
python3 code/fft_audio.py --batch white-noise.dat

# violin F
wget --continue http://freesound.org/data/previews/153/153595_2626346-lq.mp3 -O violin-F.mp3
ffmpeg -y -i violin-F.mp3 violin-F.aif
sox violin-F.aif violin-F.dat
gnuplot -e "set terminal png" -e "set output 'violin-F.png'" -e "plot 'violin-F.dat' using 1:2 with lines"
python3 code/fft_audio.py --batch violin-F.dat

# violin A
wget --continue http://freesound.org/data/previews/153/153587_2626346-lq.mp3 -O violin-A-440.mp3
ffmpeg -y -i violin-A-440.mp3 violin-A-440.aif
sox violin-A-440.aif violin-A-440.dat
gnuplot -e "set terminal png" -e "set output 'violin-A.png'" -e "plot 'violin-A-440.dat' using 1:2 with lines"
python3 code/fft_audio.py --batch violin-A-440.dat

# Pachelbel canon
wget -q --continue http://he3.magnatune.com/music/Voices%20of%20Music/Concerto%20Barocco/21-Pachelbel%20Canon%20In%20D%20Major%20-%20Johann%20Pachelbel%20-%20Canon%20And%20Gigue%20For%20Three%20Violins%20And%20Basso%20Continuo%20In%20D%20Major-Voices%20of%20Music_spoken.mp3 -O Canon.mp3
ffmpeg -y -i Canon.mp3 Canon.aif
#gnuplot -e "set terminal png" -e "set output 'Canon.png'" -e "plot 'Canon.dat' using 1:2 with lines"
#python3 code/fft_audio.py --batch Canon.dat

# a small sample of the Pachelbel canon
sox Canon.aif -t dat - | head -50000 | tail -4000 > canon-small-sample.dat
# head -50000 Canon.dat | tail -4000 > canon-small-sample.dat
python3 code/fft_audio.py --batch canon-small-sample.dat

# a sample of Mark's guitar playing
python3 code/fft_audio.py --batch mark-guitar-sample.dat

# Gloria in Excelsis Deo
wget -q --continue https://upload.wikimedia.org/wikipedia/en/e/ea/Angels_We_Have_Heard_on_High%2C_sung_by_the_Mormon_Tabernacle_Choir.ogg -O gloria.ogg
ffmpeg -y -i gloria.ogg gloria.aif
sox gloria.aif gloria.dat
gnuplot -e "set terminal png" -e "set output 'gloria.png'" -e "plot 'gloria.dat' using 1:2 with lines"
python3 code/fft_audio.py --batch gloria.dat


# calculating pi
./pi_montecarlo.py > pi_montecarlo.dat
gnuplot -e "set terminal png" -e "set output 'pi_montecarlo_approx.png'" -e "set grid" -e "plot 'pi_montecarlo.dat' using 1:4 with lines"
gnuplot -e "set terminal png lw 6" -e "set output 'pi_montecarlo_darts.png'" -e "set size square" -e "plot 'pi_montecarlo.dat' using 2:3 with points, sqrt(1 - x**2) with lines, sqrt(1 - x**2), -sqrt(1 - x**2)"
