#! /usr/bin/gnuplot
set grid
set samples 1000
set xlabel 'beta (v^2/x^2)'
set ylabel 'gamma'
plot [0:1] [0:20] 1 / sqrt(1 - x**2) lw 2
