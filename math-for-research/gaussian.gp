#! /usr/bin/gnuplot
reset
set grid
set samples 1000
set xrange [-5:5]
set yrange [0:1]
plot (1.0/(1*sqrt(2*pi))) * exp(- (1./2) * (x - 0)**2 / 1**2) title 'mean 0 sigma 1', \
 (1.0/(2*sqrt(2*pi))) * exp(- (1./2) * (x - 0)**2 / 2**2) title 'mean 0 sigma 2', \
 (1.0/(0.5*sqrt(2*pi))) * exp(- (1./2) * (x - 0)**2 / 0.5**2) title 'mean 0 sigma 0.5', \
 (1.0/(0.4*sqrt(2*pi))) * exp(- (1./2) * (x - 1.3)**2 / 0.4**2) title 'mean 1.3 sigma 0.4'
