#! /usr/bin/env python3

"""Approximate pi by shooting darts randomly into a square, that has a
circle inscribed in it."""

import random

def main():
    n_darts = 1000
    # initially we have no counts, either 
    N_cube = 0
    N_ball = 0
    print('#dart_no  x                         y                z           pi_estimate')
    for i in range(n_darts):
        N_cube += 1
        x = random.random() * 2 - 1 # radius 1, so -1 <= x <= 1
        y = random.random() * 2 - 1
        z = random.random() * 2 - 1
        if x**2 + y**2 + z**2 < 1:     # it's in the ball
            N_ball += 1
        ## we're done, so we can use the formula A_sq = 4, A_circ =
        ## pi, so pi = 4*N_circ/N_sq
        pi_estimate = (3.0/4.0) * 2**3 * float(N_ball)/float(N_cube)
        print(i, '     ', x, '   ', y, '   ', z, '   ', pi_estimate)

main()
