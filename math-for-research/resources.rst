*******************************
 Resources and further reading
*******************************

Applications of Taylor series:
==============================

https://sites.math.washington.edu/~aloveles/Math126Fall2018/m126TaylorApplicationsWorksheet.pdf

https://blog.cupcakephysics.com/relativity/2015/06/14/the-low-speed-limit-of-the-lorentz-factor.html


Differential equations
======================

Logistic differential equation:

https://en.wikipedia.org/wiki/Logistic_function#Logistic_differential_equation
