.. _chap-math-for-research-motivation:

***********************************************************
 Math for research: motivation and review of prerequisites
***********************************************************

.. _sec-math-for-research-motivation:

Motivation for "math for research"
==================================

Approximating functions and data with sums of carefully chosen
functions (like polynomials or trigonometric functions) is an almost
daily task for working scientists.  And yet I know of no curriculum
that puts these topics together before students have reached an
advanced calculus level.

Our goal here is to explore series approximations to various functions,
and in the process to look at other places in which series come up.

The most important take-home is probably that polynomials can be used
to to approximate reasonably well-behaved functions *locally*, and
Fourier series can be used to approximate reasonably well-behaved
periodic functions *globally*.

In doing so we will also get a tour of many interesting mathematical
functions, as well as physical problems that can be solved thanks to
these kinds of approximations.

We will not assume a knowledge of calculus at the start.  We will
occasionally step out to learn some speficic topics in differentition
and integration when we need it.

We will start with some mathematical *experiments* in which we start
from known series and see how well they work.  This will give us a
dissatisfaction which can be expressed as "sure, I see that it works,
but how do you get there?"

Then we will move on to learning how to generate the kinds of series
we have been experimenting with.  This will allow us to work such
approximations for many different types of situations.

..
   Purpose:
      School teaches how to solve math problems with canned
      straightforward solutions.  This is OK, but insufficient.  In
      research we come up with mathematical problems that do not have
      exact or simple solutions, so we need new techniques to deal with
      that.  These new techniques also open up new horizons for what can
      be done with math.
   In-sequence:
      This fits in the broad sequence of skills that you might call
      "practical math for research".  We talk about and use calculus
      concepts, but it is not a prerequisites - we introduce what we
      need.
   Computer use:
      Our use of computers will be extensive because we will be using
      *plotting programs* to draw functions and data.
   Programming:
      We will use simple python programs for Fourier series, signal
      processing filters, and solving differential equations
      numerically.
   Examples from:
      Environmental monitors, electronics, physics, ...


.. _sec-math-for-research-rambling-introduction:


Rambling introduction
=====================

First of all: this is a no-anxiety working group.  We get together and
do serious work, but you are always encouraged to say "hey, I did not
get get that at all - can we step back?"  You will find that everyone is
happy when we slow down or step back!  Also: if you ever feel that you
are not as good as others, you're wrong.  Everyone steps in and out of
doing well and being confused, and you will certainly be at both ends,
even if you don't notice.  That's what makes it OK to say "please, let's
step back".

As we study how to approximate things with polynomials (and other
series), we find that the traditional school model does not completely
work.  In algebra and trigonometry they have text books which have
codified the sequence in which you are supposed to study it, and they
have tons of exercises.  The books at https://openstax.org/subjects/math
for example have tons of very good exercises.

But real work bridges these codified separations, and the textbooks are
not as easy and mature.  Sometimes one has to patch together many
different sources of information, and different sources make different
assumptions on what you already know.

This means that you are taken away from that comfort zone of everything
being mapped out.  In exchange for that discomfort you get to *bridge*
areas, and your own thoughts can get profound.

And as we leave the comfort of the schoolkid approach, we replace that
with the comfort that in our working group there is no shame in stepping
back and re-tackling things from the same or different angles.

One challenge for us will be to find canned exercises for us to work
from.  Those are helpful, but harder to find as we progress.  Our
exercises might sometimes involve figuring out how to plot something, or
how to write a couple of lines of code about something.  We will move
slowly on that.

Finally: the progression I have in mind is something like this:

* sequences

* sums of numbers

* taylor polynomials

* taylor series

* fourier series

* numerical solutions to differential equations

but we will happily meander and step back to basic math when we need
more material from polymials, or trig, or exponentials, or even an
occasional flash-forward to a calculus idea.



Review of prerequisites
=======================

The main prerequisite is what we covered in the "Visualizing algebra"
working group.

We will introduce other ideas (such as derivatives and integrals) as
the need comes up.

Here are some videos and other preparation tips which give you an
interesting review, or just some attractive demonstrations.

* "3 blue 1 brown" video on Taylor series.  Note that we are not going
  to do it the way they do: they assume you know calculus, and we
  don't.  But it is visually quite lovely, so if you have a few
  minutes take a look.

https://www.youtube.com/watch?v=3d6DsjIBzJ4&t=155s

* Watch the "map of mathematics" video on youtube:
  https://www.youtube.com/watch?v=OmJ-4B-mS-Y
  revel in the British accent, and see how all areas of math fits together.

* Watch the "Professor Dave" video on sequences and sums:
  https://www.youtube.com/watch?v=-DPkqpmm1sI&list=PLybg94GvOJ9FoGQeUMFZ4SWZsr30jlUYK&index=84

* Have ready a way of calculating and plotting.  A calculator is OK, as
  is a computer calculator program.  For calculating I personally use
  "M-x calc", a reverse polish notation (RPN) calculator built in to
  emacs.  If you find RPN too nerdy then the fastest (i.e. no mouse) is
  probably to start a python3 interpreter and type expressions in to it,
  but anything you're comfortable with...  For plotting I recommend
  having gnuplot handy because it will allow us to manipulate the
  expressions we plot.
