Math and Science Working Groups
===============================

These are notes to accompany working groups I hold with driven
students who want to go beyond, or work around, the typical high
school curriculum.

Cloning
-------

The project is under mercurial version control on sourceforge.  Clone
with this instruction if you have an account on sourceforge.net:

    hg clone ssh://${LOGNAME}@hg.code.sf.net/p/math-science-working-groups/code math-science-working-groups

or this instruction to clone anonymously:

    hg clone http://hg.code.sf.net/p/math-science-working-groups/code math-science-working-groups


Build the document
------------------

The document is written in restructured text, and can be built with
the Sphinx documentation generator.  Just type:

    make html

which will place a top level index.html file in _build/html/index.html
where you can view it with your faovorite html viewer (like a web
browser).  One example sequence could be:

    make html
    firefox _build/html/index.html
