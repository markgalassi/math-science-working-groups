.. _app-how-to-build-this-book:

**********************************
 Appendix: How to build this book
**********************************

Prerequisites for building the book
===================================


You will also want some system and python packages for sphinx:

::

   sudo apt install python3-sphinx python3-sphinx-rtd-theme
   pip3 install --user --upgrade sphinx-markdown-tables
   pip3 install --user --upgrade sphinx-multitoc-numbering
   pip3 install --user --upgrade sphinxcontrib-svg2pdfconverter
   pip3 install --user --upgrade sphinxcontrib-bibtex


Cloning from codeberg.org
=========================

At https://codeberg.org/markgalassi/math-science-working-groups you
find git clone instructions:

::

   git clone https://codeberg.org/markgalassi/math-science-working-groups.git

.. note::

   If you have an account on codeberg.org you can use the "ssh URL" to
   clone the repo:

   ::

      git clone git@codeberg.org:markgalassi/math-science-working-groups.git
   
Building the book
=================

::

   cd math-science-working-group
   make html
