.. Math Summer 2020 documentation master file, created by
   sphinx-quickstart on Wed Jun  3 23:15:56 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Math and Science Working Groups
===============================

   :Date: |today|
   :Author: **Mark Galassi** <mark@galassi.org>

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :numbered:

   intro-notes.rst

.. toctree::
   :maxdepth: 1
   :caption: Part I -- Visualizing algebra
   :name: toctree-visualizing-algebra
   :numbered:

   visualizing-algebra/motivation-and-review.rst
   visualizing-algebra/introducing-symbolic-algebra.rst
   visualizing-algebra/visualizing-functions.rst
   visualizing-algebra/pantheon-of-functions.rst
   visualizing-algebra/polynomial-visualization.rst
   visualizing-algebra/solving-difficult-equations.rst
   visualizing-algebra/data-visualization.rst
   visualizing-algebra/fitting.rst
   visualizing-algebra/app-visualizing-algebra-lesson-plans.rst

.. toctree::
   :maxdepth: 1
   :caption: Part II -- Math for research
   :name: toctree-math-for-research
   :numbered:

   math-for-research/motivation-and-review.rst
   math-for-research/series.rst
   math-for-research/fourier-analysis.rst
   math-for-research/fourier-filtering.rst
   math-for-research/diffeqs.rst
   math-for-research/areas-integrals.rst
   math-for-research/monte-carlo.rst
   math-for-research/root-finding.rst
   math-for-research/resources.rst

.. toctree::
   :caption: Supplementary materials
   :name: supplementary-materials
   :numbered:

   appendix-build-this-book.rst
   glossary-terms.rst

##############
 Bibliography
##############

.. bibliography::

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
